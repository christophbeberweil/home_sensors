-- Add up migration script here
CREATE TABLE IF NOT EXISTS long_term_measurements (
    id uuid NOT NULL PRIMARY KEY DEFAULT gen_random_uuid(),
    sensor TEXT NOT NULL,
    temperature float4 NOT NULL,
    humidity float4 NOT NULL,
    created_at timestamptz NOT NULL
);