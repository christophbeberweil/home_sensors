#[derive(Debug, Clone)]
pub struct Config {
    /// connection url for the POSTGRES database
    pub database_url: String,
    pub app_port: u16,
    pub summary_interval_seconds: Option<u64>,
}

impl Config {
    pub fn init() -> Config {
        let database_url = std::env::var("DATABASE_URL").expect("DATABASE_URL must be set");
        let app_port = std::env::var("PORT").unwrap_or("3000".to_owned());
        let summary_interval_seconds_string = std::env::var("SUMMARY_INTERVAL_SECONDS");

        let summary_interval_seconds: Option<u64> = match summary_interval_seconds_string{
            Ok(siss) => Some(siss.parse::<u64>().unwrap_or(3600)),
            Err(_) => None,
        };

        
        Config {
            database_url,
            app_port: app_port.parse::<u16>().unwrap_or(3000),
            summary_interval_seconds,
        }
    }
}
