use chrono::{DateTime, Duration, Utc};
use config::Config;
use rocket::response::status::BadRequest;
use rocket::serde::{json::Json, Deserialize, Serialize};
use rocket::State;
use sqlx::postgres::PgRow;
use sqlx::PgPool;
use sqlx::{Error as SqlxError, Pool, Postgres};
use tracing::{span, Level, event, Instrument};
use tracing_subscriber::layer::SubscriberExt;
use tracing_subscriber::util::SubscriberInitExt;
use std::{net::IpAddr, str::FromStr};
use uuid::Uuid;
mod config;
use sqlx::Row;

pub trait Mean {
    fn mean(self) -> f32;
}

impl<F, T> Mean for T
where
    T: Iterator<Item = F>,
    F: std::borrow::Borrow<f32>,
{
    fn mean(self) -> f32 {
        self.zip(1..).fold(0., |s, (e, i)| {
            (*e.borrow() + s * (i - 1) as f32) / i as f32
        })
    }
}

#[derive(Serialize, Deserialize, Debug)]
#[serde(crate = "rocket::serde")]
struct RawMeasurement {
    pub temperature: f32,
    pub humidity: f32,
}

#[derive(Serialize, Deserialize, Debug)]
#[serde(crate = "rocket::serde")]
struct NewMeasurement {
    pub temperature: f32,
    pub humidity: f32,
    pub sensor: String,
}

impl NewMeasurement {
    fn from_raw_measurement(raw_measurement: RawMeasurement, sensor: String) -> Self {
        Self {
            temperature: raw_measurement.temperature,
            humidity: raw_measurement.humidity,
            sensor,
        }
    }
}

#[derive(Serialize, Deserialize, Debug, Clone)]
#[serde(crate = "rocket::serde")]
struct Measurement {
    pub id: Uuid,
    pub created_at: chrono::DateTime<chrono::Utc>,
    pub temperature: f32,
    pub humidity: f32,
    pub sensor: String,
}

#[derive(Serialize, Deserialize, Debug)]
#[serde(crate = "rocket::serde")]
struct NewLongTermMeasurement {
    pub temperature: f32,
    pub humidity: f32,
    pub sensor: String,
    pub created_at: chrono::DateTime<chrono::Utc>,
}

#[derive(Serialize, Deserialize, Debug, Clone)]
#[serde(crate = "rocket::serde")]
struct LongTermMeasurement {
    pub id: Uuid,
    pub created_at: chrono::DateTime<chrono::Utc>,
    pub temperature: f32,
    pub humidity: f32,
    pub sensor: String,
}

#[macro_use]
extern crate rocket;

#[get("/")]
fn index() -> &'static str {
    "Hello, world!"
}

#[get("/healthcheck")]
fn healthcheck() -> &'static str {
    "Online"
}

#[post(
    "/measurement/<sensor>",
    format = "application/json",
    data = "<measurement>"
)]
async fn temperature_humidity_sensor_data(
    sensor: String,
    measurement: Json<RawMeasurement>,
    pool: &State<PgPool>,
) -> Result<Json<Measurement>, BadRequest<String>> {
    let measurement = NewMeasurement::from_raw_measurement(measurement.0, sensor.clone());

    match save_measurement_to_db(measurement, pool).await {
        Ok(measurement) => {
            let message = format!("Data from {}: {:?}", &sensor, &measurement);
            tracing::debug!("{message}");
            Ok(Json(measurement))
        }
        Err(e) => {
            let message = format!("There was an error saving the data from {}!!!!", &e);
            tracing::error!("{message}");
            Err(BadRequest(message))
        }
    }
}

async fn save_measurement_to_db(
    new_measurement: NewMeasurement,
    pool: &PgPool,
) -> Result<Measurement, SqlxError> {
    match sqlx::query(
        "INSERT INTO measurements (sensor, temperature, humidity)
    VALUES ($1, $2, $3) returning *",
    )
    .bind(new_measurement.sensor)
    .bind(new_measurement.temperature)
    .bind(new_measurement.humidity)
    .map(|row: PgRow| Measurement {
        id: row.get("id"),
        created_at: row.get("created_at"),
        sensor: row.get("sensor"),
        temperature: row.get("temperature"),
        humidity: row.get("humidity"),
    })
    .fetch_one(pool)
    .await
    {
        Ok(measurement) => Ok(measurement),
        Err(e) => Err(e),
    }
}

async fn summarize_measurements(pool: &Pool<Postgres>) {
    let today = Utc::now();

    let span = span!(Level::INFO, "summarize_measurements");

    // this async block is instrumented with a span, so events are counted towards the tracing span.
    async move {
        event!(Level::INFO, "Running summarize measurements {today}");
        //tracing::info!("Running summarize measurements {today}");
    
        // target all events from yesterday and before
        let end = today - Duration::days(3);
    
        let first_datetime = sqlx::query(
            "select created_at from measurements where created_at < $1 order by created_at asc limit 1",
        )
        .bind(end)
        .map(|row: PgRow| row.get::<DateTime<Utc>, _>("created_at"))
        .fetch_one(pool)
        .await
        .ok();
    
        let sensors = sqlx::query("select distinct sensor from measurements")
            .map(|row: PgRow| row.get::<String, _>("sensor"))
            .fetch_all(pool)
            .await
            .unwrap_or(vec![]);
    
        event!(Level::DEBUG, "Sensors: {:?}", sensors);
    
        let start = match first_datetime {
            Some(first_datetime) => first_datetime - Duration::minutes(2), // offset, so that the first element is in the interval
            None => today - Duration::days(4),
        };
    
        
        event!(Level::DEBUG, "From: {start} to {end} ({} days)", (end-start).num_days());

        // define bins and select measurements (5 minutes)
    
        let duration_step = Duration::seconds(60);
        let half_duration_step = Duration::seconds(30);
        let mut s = start;
        let mut m = start + half_duration_step;
        let mut e = s + duration_step;
    
    
        while e < end {
            for sensor in &sensors {
                match sqlx::query(
                    "select * from measurements where created_at > $1 and created_at <= $2 and sensor = $3",
                )
                .bind(s)
                .bind(e)
                .bind(sensor)
                .map(|row: PgRow| Measurement {
                    id: row.get("id"),
                    created_at: row.get("created_at"),
                    sensor: row.get("sensor"),
                    temperature: row.get("temperature"),
                    humidity: row.get("humidity"),
                })
                .fetch_all(pool)
                .await
                {
                    Ok(measurements) => {
    
                        if !measurements.is_empty() {
    
                            let mean_t = measurements.iter().map(|e| e.temperature).mean();
    
                            let mean_h = measurements.iter().map(|e| e.humidity).mean();
        
        
                                
                            // save summarized measurement
        
                            let new_long_term_measurement = NewLongTermMeasurement {
                                temperature: mean_t,
                                humidity: mean_h,
                                sensor: sensor.clone(),
                                created_at: m,
                            };
        
                            match sqlx::query(
                                "INSERT INTO long_term_measurements (sensor, temperature, humidity, created_at)
                            VALUES ($1, $2, $3, $4) returning *",
                            )
                            .bind(&new_long_term_measurement.sensor)
                            .bind(new_long_term_measurement.temperature)
                            .bind(new_long_term_measurement.humidity)
                            .bind(new_long_term_measurement.created_at)
                            
                            .execute(pool)
                            .await
                            {
                                Ok(_) => {},
                                Err(e) => {tracing::error!("could not save long term measurement {new_long_term_measurement:?} {e}")},
                            };
        
                        }
    
                        // delete aggregated measurements
    
                        
                        let ids_for_deletion: Vec<Uuid> = measurements.iter().map(|e| e.id).collect();
    
                        match sqlx::query("DELETE from measurements where id = ANY($1)").bind(ids_for_deletion).execute(pool).await{
                            Ok(_) => {},
                            Err(e) => tracing::error!("Error deleting old measurements: {e}"),
                        };
                        
                    }
                    Err(e) => tracing::error!("Error: {}", e),
                };
            }
    
            s += duration_step;
            m += duration_step;
            e += duration_step;
        }
    
        event!(Level::INFO, "Done running summarize measurements {}ms", (Utc::now() - today).num_milliseconds());

    }.instrument(span).await;

    
}

#[rocket::main]
async fn main() -> Result<(), rocket::Error> {

    // init tracing setup
    tracing_subscriber::registry()
        .with(
            tracing_subscriber::EnvFilter::try_from_default_env()
                .unwrap_or_else(|_| "example_templates=debug".into()),
        )
        .with(tracing_subscriber::fmt::layer())
        .init();

    let config = Config::init();

    tracing::info!("⚙️ Config: {config:?}");
    

    let pool = sqlx::PgPool::connect(config.database_url.as_str())
        .await
        .expect("Failed to connect to database");

    let Ok(_) = sqlx::migrate!().run(&pool).await else {
        tracing::error!("Could not run migrations");
        panic!("could not run migrations!");
    };


    // summarize measurements on startup (does not work)
    // let _a = summarize_measurements(&cloned_pool).await;

    // summarize measurements periodically
    let cloned_pool = pool.clone();

    match config.summary_interval_seconds {
        Some(summary_interval_seconds) => {
            tokio::spawn(async move {

            tracing::warn!("🚀 Spawning background task");
            let mut interval = tokio::time::interval(std::time::Duration::from_secs(summary_interval_seconds));
            loop {
                let _a = summarize_measurements(&cloned_pool).await;
                interval.tick().await;
            }
        });
    },
        None => {tracing::warn!("🚀 No Background Task will be spawned");},
    };

    

    let _rocket = rocket::build()
        .manage::<PgPool>(pool)
        .configure(
            rocket::Config::figment()
                .merge((
                    "address",
                    IpAddr::from_str("0.0.0.0")
                        .expect("That the ip address to bind to could be parsed"),
                ))
                .merge(("port", config.app_port)),
        )
        .mount(
            "/",
            routes![index, healthcheck, temperature_humidity_sensor_data],
        )
        .launch()
        .await?;

    Ok(())
}
