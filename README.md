# Home Sensors

A small project to monitor my home environment.

## Project

### micropython/sensors_py
I measure the temperature and humidity with Raspberry PI Pico and a DHT22 sensor. The resulting measurements are sent to a backend server.

Deploy the main.py file to a Raspberry Pi Pico that has been initialized with MicroPython using ampy:

```
ampy -p /dev/tty.usbmodem1101 put main.py
```

After a power cycle, the new script runs.


### rocket_backend

The backend server is written in Rust using the Rocket framework. It receives the temperature and humidity measurements and stores them in a PostgreSQL database. It also manages the database migrations, which are applied on startup automatically.

Example request to the backend:
```
curl -X POST http://localhost:3000/measurement/room1 -H "Content-Type: application/json" -d '{"temperature": 19.52, "humidity": 78.53}'
```

### dev-db

Provides a convenient PostgreSQL database using docker-compose. 

