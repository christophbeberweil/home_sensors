from machine import Pin, Timer

led = Pin("LED", Pin.OUT)
tim = Timer()
def tick(timer):
    global led
    led.toggle()
    print("tick2")

tim.init(freq=2.5, mode=Timer.PERIODIC, callback=tick)