# Bibliotheken laden
import network
from machine import Pin
from time import sleep
from dht import DHT22
import ujson
import requests
import gc
ssid = 'your_ssid'
password = 'your_pw'

backend = "http://yourip:yourport"
sensor_name = "your_sensor_name"

wlan = network.WLAN(network.STA_IF)
wlan.active(True)
wlan.connect(ssid, password)

max_wait = 10
while max_wait > 0:
    if wlan.status() < 0 or wlan.status() >= 3:
        break
    max_wait -= 1
    print('waiting for connection...')
    sleep(1)

if wlan.status() != 3:
    raise RuntimeError('network connection failed')
else:
    print('connected')



    led = Pin("LED", Pin.OUT)
    led.value(1)


    # Initialisierung GPIO und DHT22
    sleep(1)
    dht22_sensor = DHT22(Pin(15, Pin.IN, Pin.PULL_UP))

    # Wiederholung (Endlos-Schleife)
    while True:
        print("---------")

        led.value(1)
        # Messung durchführen
        dht22_sensor.measure()
        # Werte lesen
        temp = dht22_sensor.temperature()
        humi = dht22_sensor.humidity()
        

        # Werte ausgeben
        print('🌡️ Temp: ', temp, '°C')
        print('💦 Hum: ', humi, '%')
        try:
            uri = f"{backend}/measurement/{sensor_name}"
            headers = {"content-type": "application/json"}
            data = {'humidity': humi, "temperature": temp}
            data = ujson.dumps(data)
            print("sending to ", uri)
            r = requests.post(uri, data=data, headers=headers)
            
            
            print("Status Code", r.status_code)
            print("✅ sent")
        except Exception as e:
            print("❌ could not send")
            print(e)
            
            for i in range(0, 10):
                led.toggle()
                sleep(0.2)
        led.value(0)
        gc.collect()

        sleep(3)